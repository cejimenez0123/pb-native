import Page from "../model/page"
import { injectable, inject } from 'inversify';
import { QuerySnapshot,DocumentData,Query } from 'firebase/firestore';

export default interface PageRepo{
    getPublicPages():Promise<QuerySnapshot<DocumentData>>
}