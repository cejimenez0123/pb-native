import { Timestamp } from "firebase/firestore";
export default interface Story {
    id:string,
    data:string,
    created:Timestamp

}