import Story from "./story"
import { Timestamp } from "firebase/firestore";

export default class Page implements Story{
    id:string
    title:string
    data:string
    profileId:string
    privacy:string
    type:string
    created:Timestamp
    constructor(
        id:string,
        title:string,
        data:string,
        profileId:string,
        privacy:string,
        type:string,
        created:Timestamp
    ){
        this.id = id
        this.title = title
        this.data = data
        this.profileId = profileId
        this.privacy = privacy
        this.type = type
        this.created = Timestamp.now();
    }
    }
    
