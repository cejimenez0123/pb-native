const catsReducer = (state = { page: [], loading: false }, action) => {
    switch(action.type) {
      case 'LOADING_CATS':
        return {
          ...state,
          pages: [...state.pages],
          loading: true
        }
      case 'ADD_CATS':
          
        return {
          ...state,
          pages: action.pages,
          loading: false
        }
      default:
        return state;
    }
  }
   
  export default catsReducer;