import '@/styles/globals.css'
import type { AppProps } from 'next/app';
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";


// import { fetchCats } from './actions/catActions';



export default function App({ Component, pageProps }: AppProps) {

  return <Component {...pageProps} />
}


// ;