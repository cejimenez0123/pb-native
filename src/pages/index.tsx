import Image from 'next/image'
import { Inter } from 'next/font/google'
import {container } from "../core/di"
import { Firestore } from "firebase/firestore"
import PageRepo  from "../domain/repository/pagerepo"
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import DashboardItem from '@/components/dashboarditem'
import Page from '@/domain/model/page';
import { Container } from 'inversify';
// useEffect(() => {
//   console.log(catPics);
//   fetchCats();
// }, [catPics, fetchCats]);
// import { fetchCats } from './actions/catActions'
const inter = Inter({ subsets: ['latin'] })

export default function Home() {
const [pages, setPages] = useState<Array<Page>>([])

  // Resolve the FooService dependency and set it to the component's state
  const pageRepo = container.get<PageRepo>('PageRepo');
  useEffect(() => {
    const loadPages = async () => {
      try {
        const querySnapshot = await pageRepo.getPublicPages();
        let pageList: Array<Page> = [];
        querySnapshot.forEach(doc => {
          const pack = doc.data();
          const { id } = doc;
          const { title, data, profileId, privacy, type, created } = pack;
          const page = new Page(id, title, data, profileId, privacy, type, created)
          pageList = [...pageList, page]
          setPages(pageList)
        })
      } catch (error) {
        console.error('Error loading pages:', error);
      }
    }
    loadPages()
  }, [])
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <form>
        <textarea id="page text field" />
        <input type="submit"/>
        {pages.map((page) => (
          DashboardItem(page)
        // <div key={page.id}>
        //   <h3>{page.title}</h3>
        //   <p style={{ color: 'White', fontSize: '24px' }}>{page.data}</p>
        // </div>
      ))}
      </form>
      
      
      <h1>HI Howoowoew?</h1>
    </main>
  )
}

// const mapStateToProps = (state) => ({
//   catPics: state.pages,
//   loading: state.loading,
// });
// const mapDispatchToProps = (dispatch) => ({
//   // getPages: () => dispatch(getPublicPages()),
//   // fetchCats: () => dispatch(fetchCats()),
// });
// export default connect(mapStateToProps, mapDispatchToProps)(App);

// export default connect(mapStateToProps, mapDispatchToProps)(App)

// Instantly deploy your Next.js site to a shareable URL with Vercel.