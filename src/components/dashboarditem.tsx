import Page from '../domain/model/page';
import {PageType} from "../core/constants"
export default function DashboardItem(page:Page){
  
    let pageDataElement;
    switch(page.type){
        case PageType.text:
            pageDataElement = <div dangerouslySetInnerHTML={{__html:page.data}}></div>
        break;
        case PageType.picture:
            pageDataElement = <img src={page.data}/>
        break;
        case PageType.video:
            pageDataElement = <video src={page.data}/>
        break;
        default:
            pageDataElement = <div dangerouslySetInnerHTML={{__html:page.data}}/>
        break;
    }
    return (
        <div className="dashboard-item">
            <div className="dashboard-item-header">
                <div className="dashboard-item-title">{page.title}</div>
                <div className="dashboard-item-type">{}</div>
            </div>
            <div className="dashboard-item-content">
                <div className="dashboard-item-content-title">{page.title}</div>
                {pageDataElement}
            </div>
        </div>
    )
  

}