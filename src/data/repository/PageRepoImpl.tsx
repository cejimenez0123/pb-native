import Page from "@/domain/model/page"
import {Firestore , QuerySnapshot, DocumentData } from 'firebase/firestore';
import {where,query,collection,getDocs} from "firebase/firestore"
import PageRepo from "@/domain/repository/pagerepo"
import { injectable, inject } from 'inversify';


export default class PageRepoImpl implements PageRepo{

    private pbDatabase:Firestore;
    public constructor(db:  Firestore){
        
        this.pbDatabase = db;
    }
    public async getPublicPages(): Promise<QuerySnapshot<DocumentData>>{
        const snapshot = getDocs(query(collection(this.pbDatabase, "page"), where("privacy", "==", false)))
        return snapshot
    }

}

