import { Container } from 'inversify';

import { initializeApp } from "firebase/app";
import { getFirestore,Firestore,initializeFirestore} from "firebase/firestore";
import  PageRepo  from "@/domain/repository/pagerepo"
import PageRepoImpl from '@/data/repository/PageRepoImpl';

const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DATABASE_URL,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID,
  appId: process.env.APP_ID,
  measurementId: process.env.MEASUREMENT_ID
};
const app = initializeApp(firebaseConfig);
interface DatabaseService {
    
    getFirestoreInstance(): Firestore;
  }

// Initialize Cloud Firestore and get a reference to the service
const db = initializeFirestore(app, {
    experimentalForceLongPolling: true, // this line
    useFetchStreams: false, // and this line
  })

const container = new Container();
container.bind('pbDatabase').toConstantValue(
     db
  );
container.bind<PageRepo>('PageRepo').toDynamicValue((context) => {
    return new PageRepoImpl(db);
  });
export { container };